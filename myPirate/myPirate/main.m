//
//  main.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XYZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XYZAppDelegate class]));
    }
}
