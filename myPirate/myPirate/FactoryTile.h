//
//  FactoryTile.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tile.h"

@interface FactoryTile : NSObject

-(NSMutableArray *) getTilesArray;

@end
