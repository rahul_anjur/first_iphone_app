//
//  Armor.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "Armor.h"

@implementation Armor


-(Armor *) giveMeArmor{
    
    Armor *armor =[[Armor alloc] init];
    
    Armor *armor1 =[[Armor alloc] init];
    armor1.armorName = @"Shield";
    armor1.armorProtection = 5;
    
    Armor *armor2 = [[Armor alloc] init];
    armor2.armorName = @"Tank";
    armor2.armorProtection = 10;
    
    
    Armor *armor3 = [[Armor alloc] init];
    armor3.armorName = @"GOD";
    armor3.armorProtection=16;
    
    
    NSArray *armorArray = [NSArray arrayWithObjects:armor1,armor2,armor3, nil];
    
    NSUInteger randomIndex = arc4random() % [armorArray count];
    armor= [armorArray objectAtIndex: randomIndex];
    
    
    return armor ;
}

@end
