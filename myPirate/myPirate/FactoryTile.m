//
//  FactoryTile.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "FactoryTile.h"

@implementation FactoryTile

-(NSMutableArray *) getTilesArray{
    
    NSMutableArray *finalTiles =[[NSMutableArray alloc] init ];
    
    NSMutableArray *zeroCord = [[NSMutableArray alloc] init ];
    NSMutableArray *oneCord = [[NSMutableArray alloc] init ];
    NSMutableArray *twoCord = [[NSMutableArray alloc] init ];
    NSMutableArray *threeCord = [[NSMutableArray alloc] init ];
    
    Tile *tile00 = [[Tile alloc] init];
    Tile *tile01 = [[Tile alloc] init];
    Tile *tile02 = [[Tile alloc] init];
    
    Tile *tile10 = [[Tile alloc] init];
    Tile *tile11 = [[Tile alloc] init];
    Tile *tile12 = [[Tile alloc] init];
    
    Tile *tile20 = [[Tile alloc] init];
    Tile *tile21 = [[Tile alloc] init];
    Tile *tile22 = [[Tile alloc] init];
    
    Tile *tile30 = [[Tile alloc] init];
    Tile *tile31 = [[Tile alloc] init];
    Tile *tile32 = [[Tile alloc] init];
    
    
    
    
    NSString *story1 = @"Welcome to the pirate game, In the harsh sea's welcome to the great adventure";
    NSString *story2 = @"A big whale ahead";
    NSString *story3 = @"The seas getting bad , do something "; // stabalize ship button
    NSString *story4 = @" Beautiful sunset in the east";
    NSString *story5 = @"BOSSSS .. nuff said";
    NSString *story6 = @"Suffering from a bout of scurvy";
    NSString *story7 = @"you have to walk the plank , too bad for you";
    NSString *story8 = @"Another pirate ship move forward fast ";
    NSString *story9 = @"The pirate ship is blocking you , need to fight";
    NSString *story10 = @"Head west?";
    NSString *story11 = @"Lochness monster ahead";
    NSString *story12 =@"It's been a good game";
    
    
    tile00.story = story1;
    tile00.backgroundImage = [UIImage imageNamed:@"PirateStart.jpg"];
    tile00.action = @"Start";
    tile00.hasBoss = false;
    tile00.hasWeapon = false;
    
    
    
    tile01.story = story2;
    tile01.backgroundImage = [UIImage imageNamed:@"PirateParrot.jpg"];
    tile01.action = @"Harpoon";
    tile01.hasBoss = false;
    tile01.hasWeapon = false;
    
    tile02.story = story3;
    tile02.backgroundImage = [UIImage imageNamed:@"PirateOctopusAttack.jpg"];
    tile02.action =@"Stabalize";
    tile02.hasBoss = false;
    tile02.hasWeapon = true;
    
    
    tile10.story = story4;
    tile10.backgroundImage =[UIImage imageNamed:@"PirateTreasure.jpeg"];
    tile10.action  = @"Enjoy";
    tile10.hasBoss = false;
    tile10.hasWeapon = false;
    
    
    tile11.story = story5;
    tile11.backgroundImage = [UIImage imageNamed:@"PirateBoss.jpeg"];
    tile11.action = @"Fight";
    tile11.hasBoss = true;
    tile11.hasWeapon = true;
    
    
    
    
    tile12.story = story6;
    tile12.backgroundImage = [UIImage imageNamed:@"PirateTreasurer2.jpeg"];  // Should be a time challenge
    tile12.action = @"vitaminC";
    tile12.hasBoss = false;
    tile12.hasWeapon = false;
    
    
    
    tile20.story = story7;
    tile20.backgroundImage = [UIImage imageNamed:@"PiratePlank.jpg"];
    tile20.action = @"Ooops";
    tile20.hasBoss = false;
    tile20.hasWeapon = false;
    
    
    
    tile21.story = story8;
    tile21.backgroundImage = [UIImage imageNamed:@"PirateWeapons.jpeg"];
    tile21.action = @"Fight Em";
    tile21.hasBoss = false;
    tile21.hasWeapon =true;
    
    
    
    tile22.story = story9;
    tile22.backgroundImage = [UIImage imageNamed:@"PirateFriendlyDock.jpg"];
    tile22.action = @"Kill em";
    tile22.hasBoss = false;
    tile22.hasWeapon = false;
    
    
    
    tile30.story = story10;
    tile30.backgroundImage =[UIImage imageNamed:@"PirateBlacksmith.jpeg"];
    tile30.action = @"Sure";
    tile30.hasBoss = false;
    tile30.hasWeapon = false;
    
    
    
    tile31.story = story11;
    tile31.backgroundImage = [UIImage imageNamed:@"PirateShipBattle.jpeg"];
    tile31.action = @"the horror";
    tile31.hasBoss = false;
    tile31.hasWeapon = false;
    
    
    
    tile32.story =story12;
    tile32.backgroundImage = [UIImage imageNamed:@"PirateAttack.jpg"];
    tile32.action = @"Thanks for playing";
    tile32.hasBoss = false;
    tile32.hasWeapon = false;
    
    
    
    [zeroCord insertObject:tile00 atIndex:0];
    [zeroCord insertObject:tile01 atIndex:1];
    [zeroCord insertObject:tile02 atIndex:2];
    
    [oneCord insertObject:tile10 atIndex:0];
    [oneCord insertObject:tile11 atIndex:1];
    [oneCord insertObject:tile12 atIndex:2];
    
    [twoCord insertObject:tile20 atIndex:0];
    [twoCord insertObject:tile21 atIndex:1];
    [twoCord insertObject:tile22 atIndex:2];
    
    [threeCord insertObject:tile30 atIndex:0];
    [threeCord insertObject:tile31 atIndex:1];
    [threeCord insertObject:tile32 atIndex:2];
    
    [finalTiles insertObject:zeroCord atIndex:0];
    [finalTiles insertObject:oneCord atIndex:1];
    [finalTiles insertObject:twoCord atIndex:2];
    [finalTiles insertObject:threeCord atIndex:3];
    
    
    
    
    // NSMutableArray *storyArray=
    
    return finalTiles;
    
}
@end
