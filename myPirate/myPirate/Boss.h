//
//  Boss.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Boss : NSObject

@property NSString *bossName;
@property int bossDamage;
@property int bossLife;
@property int bossArmor;

-(Boss *) returnBossStats;


@end
