//
//  XYZViewController.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FactoryTile.h"
#import "Weapon.h"
#import "Armor.h"
#import "Character.h"
#import "Boss.h"

@interface XYZViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *healthLabel;
@property (strong, nonatomic) IBOutlet UILabel *damageLabel;
@property (strong, nonatomic) IBOutlet UILabel *weaponLabel;
@property (strong, nonatomic) IBOutlet UILabel *armorLabel;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UILabel *storyLabel;
@property (strong, nonatomic) IBOutlet UILabel *TileWeaponName;
@property (strong, nonatomic) IBOutlet UILabel *bossHealth;
@property (strong, nonatomic) IBOutlet UILabel *characterName;
@property (strong, nonatomic) IBOutlet UIButton *northButton;
@property (strong, nonatomic) IBOutlet UIButton *westButton;
@property (strong, nonatomic) IBOutlet UIButton *eastButton;
@property (strong, nonatomic) IBOutlet UIButton *southButton;
@property (strong, nonatomic) IBOutlet UIButton *actionButton;
@property (strong, nonatomic) IBOutlet UILabel *armorName;
@property (strong, nonatomic) IBOutlet UIButton *pickUpWeapon;

@property Boss *tileBoss;
@property FactoryTile *factoryTile;
@property int presentBossHealth;
@property int presentCharHealth ;
@property int presentBossDamage;
@property int presentCharDamage;
@property int tileWeaponDamage;
@property NSString *tilWeaponName;
@property Tile *tile1;
@property Tile *tile2;
@property Tile *tile3;
@property Tile *tile4;
@property Tile *tile5;
@property Tile *tile6;
@property Tile *tile7;
@property Tile *tile8;
@property Tile *tile9;
@property Tile *tile10;
@property Tile *tile11;
@property Tile *tile12;
@property Tile *presentTile;
@property Weapon *tileWeapon;
@property Armor *tileArmor;
@property CGPoint presentCords;
@property CGPoint pastCords;
@property Character *character;

- (IBAction)weaponPickup:(id)sender;
- (IBAction)actionButton:(id)sender;
- (IBAction)northButton:(id)sender;
- (IBAction)eastButton:(id)sender;
- (IBAction)southButton:(id)sender;
- (IBAction)westButton:(id)sender;

@end
