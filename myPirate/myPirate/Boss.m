//
//  Boss.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "Boss.h"

@implementation Boss

-(Boss *) returnBossStats{
    Boss *boss = [[Boss alloc] init];
    
    boss.bossName = @"Russian";
    boss.bossArmor = 20;
    boss.bossLife = 40;
    boss.bossDamage = 10;
    
    return boss;
    
    
}

@end
