//
//  Weapon.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "Weapon.h"

@implementation Weapon

-(Weapon *) returnAWeapon{
    Weapon *weapon = [[Weapon alloc] init];
    
    Weapon *weapon1 = [[Weapon alloc] init];
        weapon1.weaponName = @"sword";
    weapon1.weaponDamage =4;
    
    Weapon *weapon2 = [[Weapon alloc] init];
    weapon2.weaponName = @"gun";
    weapon2.weaponDamage = 9;
    
    
    Weapon *weapon3 = [[Weapon alloc] init];
    weapon3.weaponName = @"BFG";
    weapon3.weaponDamage = 15;
    
    
    NSArray *weaponsArray = [NSArray arrayWithObjects:weapon1,weapon2,weapon3, nil];
    NSUInteger randomIndex = arc4random() % [weaponsArray count];
    weapon= [weaponsArray objectAtIndex: randomIndex];
    
    
    return weapon;

    
    
}

@end
