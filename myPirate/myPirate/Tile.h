//
//  Tile.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tile : NSObject

@property NSString *story;  // for every story
@property UIImage *backgroundImage;  // for background image
@property BOOL hasWeapon;   // checks if there is a new a weapon
@property BOOL hasBoss;   // checks if there is a boss
@property NSString *action;  // Checks the action that should happen
@end
