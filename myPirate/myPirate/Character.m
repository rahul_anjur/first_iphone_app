//
//  Character.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "Character.h"

@implementation Character

-(Character *) getInitChar{
    
    Character *character =[[Character alloc] init];
    
    character.charName = @"Haddock";
    character.charLife = 50;
    
    Weapon *initWeapon =[[Weapon alloc] init];
    initWeapon.weaponName = @"Fists";
    initWeapon.weaponDamage = 2;
    
    character.presentWeapon = initWeapon;
    
    Armor *initArmor =[[Armor alloc] init];
    initArmor.armorName = @"drum";
    initArmor.armorProtection=4;
    
    character.presentArmor = initArmor;
    
    
    
    return character;
    
}

@end
