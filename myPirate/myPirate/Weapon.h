//
//  Weapon.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weapon : NSObject

@property NSString *weaponName ;
@property int weaponDamage;

-(Weapon *) returnAWeapon;


@end
