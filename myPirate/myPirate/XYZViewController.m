//
//  XYZViewController.m
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "XYZViewController.h"
#import "FactoryTile.h"
#import "Boss.h"


@interface XYZViewController ()

@end

@implementation XYZViewController

- (void)viewDidLoad

{
    [super viewDidLoad];
    self.backgroundImage.image = [UIImage imageNamed:@"PirateAttack.jpg"];

    FactoryTile *factoryTile =[[FactoryTile alloc] init];
    NSMutableArray *array =[[NSMutableArray alloc] init];
    Tile *array1 =[[Tile alloc] init];
    NSMutableArray *array2 =[[NSMutableArray alloc] init];
    NSMutableArray *array3 =[[NSMutableArray alloc] init];
    NSMutableArray *array4 = [[NSMutableArray alloc] init];
    NSMutableArray *array5 = [[NSMutableArray alloc] init];
    array = [factoryTile getTilesArray]; // Getting the array
    array2 =[array objectAtIndex:0];
    array3 = [array objectAtIndex:1];
    array4 = [array objectAtIndex:2];
    array5 = [array objectAtIndex:3];
    
    array1 = [array2 objectAtIndex:0];
    NSLog(@"array : %@",array1);
    self.backgroundImage.image = array1.backgroundImage;
    self.storyLabel.text = array1.story;
    //self.northButton.hidden = true;
    
    self.tile1 = [array2 objectAtIndex:0];
    self.tile2 = [array2 objectAtIndex:1];
    self.tile3 = [array2 objectAtIndex:2];
    
    self.tile4 = [array3 objectAtIndex:0];
    self.tile5 = [array3 objectAtIndex:1];
    self.tile6 = [array3 objectAtIndex:2];
    
    self.tile7 = [array4 objectAtIndex:0];
    self.tile8 = [array4 objectAtIndex:1];
    self.tile9 = [array4 objectAtIndex:2];
    
    self.tile10 = [array5 objectAtIndex:0];
    self.tile11 = [array5 objectAtIndex:1];
    self.tile12 = [array5 objectAtIndex:2];
    
    self.presentCords = CGPointMake(0, 0);
    self.westButton.hidden = true;
    self.southButton.hidden = true;
    [_actionButton setTitle:self.tile1.action forState:UIControlStateNormal];
    
   // setting character attributes
    Character *getcharacter = [[Character alloc]init];
    self.character = [[Character alloc] init];
    self.character = [getcharacter getInitChar];
    
    Weapon *present = [[Weapon alloc] init];
    present = self.character.presentWeapon;
    
    Armor *presentArmor = [[Armor alloc] init];
    presentArmor = self.character.presentArmor;
    
    self.bossHealth.hidden = YES; // hiding the boss button
  
    self.TileWeaponName.hidden = YES; // hiding the new weapon name
    
    // Setting the character weapon and armor attributes
    self.characterName.text = self.character.charName;
    self.healthLabel.text = [@(self.character.charLife) stringValue];
    self.weaponLabel.text = present.weaponName;
    self.damageLabel.text = [@(present.weaponDamage) stringValue];
    
    self.armorLabel.text = [@(presentArmor.armorProtection) stringValue];
    self.armorName.text = presentArmor.armorName;
    
    // setting the boss
    
    Boss *boss = [[Boss alloc] init];
    self.tileBoss =[boss returnBossStats];
    self.bossHealth.text = [@(self.tileBoss.bossLife) stringValue];
    
    // setting the health for the boss battle
    
    int armorToHealthRatio = 1; // CHange these stats for better battles
    
    self.presentBossHealth = self.tileBoss.bossLife + armorToHealthRatio*(self.tileBoss.bossArmor);
    self.presentCharHealth = self.character.charLife + armorToHealthRatio*(self.character.presentArmor.armorProtection);
    
    self.presentBossDamage = self.tileBoss.bossDamage;
    self.presentCharDamage = self.character.presentWeapon.weaponDamage;
    
    //setting the pickup weapon
    
    self.pickUpWeapon.hidden =YES;
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)northButton:(id)sender {
    
    // each time you move north , you have to update the Y cord by one
    // make sure you have the boundary check .
    self.bossHealth.hidden = YES;
    self.TileWeaponName.hidden = YES;
    self.pickUpWeapon.hidden =NO;

    
    if(self.presentCords.y < 2 ){
        self.northButton.hidden = false;
    int cord = self.presentCords.y +1;
    self.presentCords = CGPointMake(self.presentCords.x, cord);
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
        if(self.presentCords.y==2){
            
          self.northButton.hidden = true;
        }
        
}else
{
    self.northButton.hidden = true;
    self.presentCords = CGPointMake(self.presentCords.x, self.presentCords.y);
    NSLog(@"%@",NSStringFromCGPoint(self.presentCords));}


    if(self.presentCords.y!=0){
        
        self.southButton.hidden = false;
    }
    
    // code to update the views should be put up here
    
    if(self.presentCords.x==0){
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile2.backgroundImage;
            self.storyLabel.text = self.tile2.story;
            [_actionButton setTitle:self.tile2.action forState:UIControlStateNormal];

            if(self.tile2.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                // add
                
            }
            
            if(self.tile2.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }
        
        if(self.presentCords.y==2){
            [_actionButton setTitle:self.tile3.action forState:UIControlStateNormal];

            self.backgroundImage.image = self.tile3.backgroundImage;
            self.storyLabel.text = self.tile3.story;
            if(self.tile3.hasBoss==true){
                self.bossHealth.hidden = NO;
                // add
                
            }
            if(self.tile3.hasWeapon){
                
                self.pickUpWeapon.hidden =NO;
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }
            
            
            
        }

        
        
        
    }
    
    
    if(self.presentCords.x==1){
        
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile5.backgroundImage;
            self.storyLabel.text = self.tile5.story;
            [_actionButton setTitle:self.tile5.action forState:UIControlStateNormal];

            if(self.tile5.hasBoss==true){
                
                self.bossHealth.hidden = NO;

                
                // add
                
            }
        
            if(self.tile5.hasWeapon){
                self.pickUpWeapon.hidden =NO;
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }
        
        if(self.presentCords.y==2){
            
            self.backgroundImage.image = self.tile6.backgroundImage;
            self.storyLabel.text = self.tile6.story;
            [_actionButton setTitle:self.tile6.action forState:UIControlStateNormal];

            if(self.tile6.hasBoss==true){
                self.bossHealth.hidden = NO;

                // add
                
            }
            if(self.tile6.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
            
            
        }
        
    }
    
    if(self.presentCords.x==2){
        
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile8.backgroundImage;
            self.storyLabel.text = self.tile8.story;
            [_actionButton setTitle:self.tile8.action forState:UIControlStateNormal];

            if(self.tile8.hasBoss==true){
                self.bossHealth.hidden = NO;

                // add
                
            }
            
            if(self.tile8.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }

            
            
        }
        
        
        if(self.presentCords.y==2){
            
            self.backgroundImage.image = self.tile9.backgroundImage;
            self.storyLabel.text = self.tile9.story;
            [_actionButton setTitle:self.tile9.action forState:UIControlStateNormal];

            if(self.tile9.hasBoss==true){
                self.bossHealth.hidden = NO;

                // add
                
            }
            
            if(self.tile9.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }

            
            
        }
        

        
    }
    
    //
    if(self.presentCords.x==3){
        
        
        if(self.presentCords.y==1){
            NSLog(@"here");
            
            self.backgroundImage.image = self.tile11.backgroundImage;
            self.storyLabel.text = self.tile11.story;
            [_actionButton setTitle:self.tile11.action forState:UIControlStateNormal];

            if(self.tile11.hasBoss==true){
                self.bossHealth.hidden = NO;

                // add
                
            }
            
            if(self.tile11.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
               self.TileWeaponName.hidden = NO;
               self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }

            
            
        }
        
        
        if(self.presentCords.y==2){
            NSLog(@"here");
            
            self.backgroundImage.image = self.tile12.backgroundImage;
            self.storyLabel.text = self.tile12.story;
            [_actionButton setTitle:self.tile12.action forState:UIControlStateNormal];

            if(self.tile12.hasBoss==true){
                self.bossHealth.hidden = NO;

                // add
                
            }
            
            if(self.tile12.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }

            
            
        }
        
        
        
    }

    

} // end of north button function

- (IBAction)eastButton:(id)sender {
    
    self.bossHealth.hidden = YES;
    self.TileWeaponName.hidden = YES;
    self.pickUpWeapon.hidden =YES;


    
    if(self.presentCords.x < 3 ){
        
        self.eastButton.hidden = false;
        int cord = self.presentCords.x +1;
        
        self.presentCords = CGPointMake(cord , self.presentCords.y);
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
        if(self.presentCords.x ==3){
            self.eastButton.hidden = true;
            
        }
    }else
    {
        self.presentCords = CGPointMake(self.presentCords.x, self.presentCords.y);
        self.eastButton.hidden = true;
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));    }
    
    if(self.presentCords.x!=0){
        
        self.westButton.hidden = false;
    }
    // starting nav logic
    
    if(self.presentCords.y == 0){
        
        if(self.presentCords.x==1){
            
            self.backgroundImage.image = self.tile4.backgroundImage;
            self.storyLabel.text = self.tile4.story;
            [_actionButton setTitle:self.tile4.action forState:UIControlStateNormal];

            if(self.tile4.hasBoss==true){
                self.bossHealth.hidden = NO;
                
 
                
            }
            if(self.tile4.hasWeapon){
                self.pickUpWeapon.hidden =YES;

                
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }
            
            
        }// end
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile7.backgroundImage;
            self.storyLabel.text = self.tile7.story;
            [_actionButton setTitle:self.tile7.action forState:UIControlStateNormal];

            if(self.tile7.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
                }
            
            if(self.tile7.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
         
                
            }

            
        } // end
        
        
        if(self.presentCords.x==3){
            
            self.backgroundImage.image = self.tile10.backgroundImage;
            self.storyLabel.text = self.tile10.story;
            [_actionButton setTitle:self.tile10.action forState:UIControlStateNormal];

            if(self.tile10.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile10.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }

    
    }//end
    
    if(self.presentCords.y == 2){
        
        if(self.presentCords.x==1){
            
            self.backgroundImage.image = self.tile6.backgroundImage;
            self.storyLabel.text = self.tile6.story;
            [_actionButton setTitle:self.tile6.action forState:UIControlStateNormal];

            if(self.tile6.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
            }
            if(self.tile6.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }// end
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile9.backgroundImage;
            self.storyLabel.text = self.tile9.story;
            [_actionButton setTitle:self.tile9.action forState:UIControlStateNormal];

            if(self.tile9.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile9.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
        if(self.presentCords.x==3){
            
            self.backgroundImage.image = self.tile12.backgroundImage;
            self.storyLabel.text = self.tile12.story;
            [_actionButton setTitle:self.tile12.action forState:UIControlStateNormal];

            if(self.tile12.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile12.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }
        
    }//end
    
    
    if(self.presentCords.y == 1){
        
        if(self.presentCords.x==1){
            
            self.backgroundImage.image = self.tile5.backgroundImage;
            self.storyLabel.text = self.tile5.story;
            [_actionButton setTitle:self.tile5.action forState:UIControlStateNormal];

            if(self.tile5.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
            }
            if(self.tile5.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }// end
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile8.backgroundImage;
            self.storyLabel.text = self.tile8.story;
            [_actionButton setTitle:self.tile8.action forState:UIControlStateNormal];

            if(self.tile8.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile8.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
        if(self.presentCords.x==3){
            
            self.backgroundImage.image = self.tile11.backgroundImage;
            self.storyLabel.text = self.tile11.story;
            [_actionButton setTitle:self.tile11.action forState:UIControlStateNormal];

            if(self.tile11.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile11.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }
        
    }//end
    
    
    
    

    
}// end of east button

- (IBAction)southButton:(id)sender {
    self.bossHealth.hidden = YES;
    self.TileWeaponName.hidden = YES;
    self.pickUpWeapon.hidden =YES;


    
    if(self.presentCords.y > 0 ){
        self.southButton.hidden = false;
        int cord = self.presentCords.y -1;
        
        self.presentCords = CGPointMake(self.presentCords.x, cord);
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
        
        if(self.presentCords.y==0){
            self.southButton.hidden = true;
    
            
    }
        
        
    }else
    {
        self.presentCords = CGPointMake(self.presentCords.x, self.presentCords.y);
        self.southButton.hidden = true;
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
    }
    
    
    if(self.presentCords.y!=2){
        
        self.northButton.hidden = false;

    }
    
    // Navigation Control in South Button
    
    if(self.presentCords.x == 0){
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile2.backgroundImage;
            self.storyLabel.text = self.tile2.story;
            [_actionButton setTitle:self.tile2.action forState:UIControlStateNormal];

            if(self.tile2.hasBoss==true){
                self.bossHealth.hidden = NO;
                
  
            
            
            
            
        }
            if(self.tile2.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }

        
        }// end
        
        if(self.presentCords.y==0){
            
            self.backgroundImage.image = self.tile1.backgroundImage;
            self.storyLabel.text = self.tile1.story;
            [_actionButton setTitle:self.tile1.action forState:UIControlStateNormal];

            if(self.tile1.hasBoss==true){
                self.bossHealth.hidden = NO;
                
  
                
                if(self.tile1.hasWeapon){
                    self.pickUpWeapon.hidden =NO;

                    Weapon *weapon = [[Weapon alloc] init];
                    weapon =[weapon returnAWeapon];
                    self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                    self.tileWeaponDamage = weapon.weaponDamage;
                    self.tilWeaponName = weapon.weaponName;
                    
                    
                }

            
        }// end
        
        
        
        
        
    }
    

    
}
    if(self.presentCords.x == 1){
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile5.backgroundImage;
            self.storyLabel.text = self.tile5.story;
            [_actionButton setTitle:self.tile5.action forState:UIControlStateNormal];

            if(self.tile5.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
                
                
            }
            if(self.tile5.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }// end
        
        if(self.presentCords.y==0){
            
            self.backgroundImage.image = self.tile4.backgroundImage;
            self.storyLabel.text = self.tile4.story;
            [_actionButton setTitle:self.tile4.action forState:UIControlStateNormal];

            if(self.tile4.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
                if(self.tile4.hasWeapon){
                    self.pickUpWeapon.hidden =NO;

                    Weapon *weapon = [[Weapon alloc] init];
                    weapon =[weapon returnAWeapon];
                    self.TileWeaponName.hidden = NO;
                    self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                    
                    self.tileWeaponDamage = weapon.weaponDamage;
                    self.tilWeaponName = weapon.weaponName;
                    
                }
                
                
            }// end
            
            
            
            
            
        }
        
        
        
    }
    if(self.presentCords.x == 2){
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile8.backgroundImage;
            self.storyLabel.text = self.tile8.story;
            [_actionButton setTitle:self.tile8.action forState:UIControlStateNormal];

            if(self.tile8.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
                
                
                
            }
            if(self.tile8.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }// end
        
        if(self.presentCords.y==0){
            
            self.backgroundImage.image = self.tile7.backgroundImage;
            self.storyLabel.text = self.tile7.story;
            [_actionButton setTitle:self.tile7.action forState:UIControlStateNormal];

            if(self.tile7.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
                if(self.tile7.hasWeapon){
                    self.pickUpWeapon.hidden =NO;

                    Weapon *weapon = [[Weapon alloc] init];
                    weapon =[weapon returnAWeapon];
                    self.TileWeaponName.hidden = NO;
                    self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                    self.tileWeaponDamage = weapon.weaponDamage;
                    self.tilWeaponName = weapon.weaponName;
                    
                    
                }
                
                
            }// end
            
            
            
            
            
        }
        
        
        
    }
    
    if(self.presentCords.x == 3){
        
        if(self.presentCords.y==1){
            
            self.backgroundImage.image = self.tile11.backgroundImage;
            self.storyLabel.text = self.tile11.story;
            [_actionButton setTitle:self.tile11.action forState:UIControlStateNormal];

            if(self.tile11.hasBoss==true){
                self.bossHealth.hidden = NO;
                
    
                
            }
            if(self.tile11.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        }// end
        
        if(self.presentCords.y==0){
            
            self.backgroundImage.image = self.tile10.backgroundImage;
            self.storyLabel.text = self.tile10.story;
            [_actionButton setTitle:self.tile10.action forState:UIControlStateNormal];

            if(self.tile10.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
                if(self.tile10.hasWeapon){
                    self.pickUpWeapon.hidden =NO;

                    Weapon *weapon = [[Weapon alloc] init];
                    weapon =[weapon returnAWeapon];
                    self.TileWeaponName.hidden = NO;
                    self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                    self.tileWeaponDamage = weapon.weaponDamage;
                    self.tilWeaponName = weapon.weaponName;
                    
                    
                }
                
                
            }
            
        }
        
            
            
        }// end
}// end of south button

- (IBAction)westButton:(id)sender {
    self.bossHealth.hidden = YES;
    self.TileWeaponName.hidden = YES;
    self.pickUpWeapon.hidden =YES;


    
    if(self.presentCords.x >0 ){
        
        self.westButton.hidden = false;
        int cord = self.presentCords.x -1;
        
        self.presentCords = CGPointMake(cord , self.presentCords.y);
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
        if(self.presentCords.x==0){
            self.westButton.hidden = true;
            
            
        }
    }
    else
    {
        self.presentCords = CGPointMake(self.presentCords.x, self.presentCords.y);
        self.westButton.hidden = true;
        NSLog(@"%@",NSStringFromCGPoint(self.presentCords));
    }
    
    
    if(self.presentCords.x!=3){
        
        self.eastButton.hidden = false;
    }
    
    
    if(self.presentCords.y == 0){
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile7.backgroundImage;
            self.storyLabel.text = self.tile7.story;
            [_actionButton setTitle:self.tile7.action forState:UIControlStateNormal];

            if(self.tile7.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
            }
            if(self.tile7.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                
                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }
            
            
        }// end
        
        if(self.presentCords.x==1){
            
            self.backgroundImage.image = self.tile4.backgroundImage;
            self.storyLabel.text = self.tile4.story;
            [_actionButton setTitle:self.tile4.action forState:UIControlStateNormal];

            if(self.tile4.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile4.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
        if(self.presentCords.x==0){
            
            self.backgroundImage.image = self.tile1.backgroundImage;
            self.storyLabel.text = self.tile1.story;
            [_actionButton setTitle:self.tile1.action forState:UIControlStateNormal];

            if(self.tile1.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile1.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        

    } // end of cords
    
    if(self.presentCords.y == 1){
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile8.backgroundImage;
            self.storyLabel.text = self.tile8.story;
            [_actionButton setTitle:self.tile8.action forState:UIControlStateNormal];

            if(self.tile8.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
            }
            if(self.tile8.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }
            
            
        }// end
        
        if(self.presentCords.x==1){

            self.backgroundImage.image = self.tile5.backgroundImage;
            self.storyLabel.text = self.tile5.story;
            [_actionButton setTitle:self.tile5.action forState:UIControlStateNormal];

            if(self.tile5.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile5.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
        if(self.presentCords.x==0){
            
            self.backgroundImage.image = self.tile2.backgroundImage;
            self.storyLabel.text = self.tile2.story;
            [_actionButton setTitle:self.tile2.action forState:UIControlStateNormal];

            if(self.tile2.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile2.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
    }
    
    if(self.presentCords.y == 2){
        
        if(self.presentCords.x==2){
            
            self.backgroundImage.image = self.tile9.backgroundImage;
            self.storyLabel.text = self.tile9.story;
            [_actionButton setTitle:self.tile9.action forState:UIControlStateNormal];

            if(self.tile9.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                
            }
            if(self.tile9.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
            }
            
            
        }// end
        
        if(self.presentCords.x==1){
            
            self.backgroundImage.image = self.tile6.backgroundImage;
            self.storyLabel.text = self.tile6.story;
            [_actionButton setTitle:self.tile6.action forState:UIControlStateNormal];

            if(self.tile6.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile6.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
        if(self.presentCords.x==0){
            
            self.backgroundImage.image = self.tile3.backgroundImage;
            self.storyLabel.text = self.tile3.story;
            [_actionButton setTitle:self.tile3.action forState:UIControlStateNormal];

            if(self.tile3.hasBoss==true){
                self.bossHealth.hidden = NO;
                
                
                // add boss logic
                
            }
            
            if(self.tile3.hasWeapon){
                self.pickUpWeapon.hidden =NO;

                Weapon *weapon = [[Weapon alloc] init];
                weapon =[weapon returnAWeapon];
                self.TileWeaponName.hidden = NO;
                self.TileWeaponName.text =[weapon.weaponName stringByAppendingString:[@(weapon.weaponDamage) stringValue]];
                self.tileWeaponDamage = weapon.weaponDamage;
                self.tilWeaponName = weapon.weaponName;
                
                
            }
            
            
        } // end
        
        
    }
    
    
 
    
}// end of west button.



    

- (IBAction)weaponPickup:(id)sender {
    self.presentCharDamage = self.tileWeaponDamage;
    self.damageLabel.text = [NSString stringWithFormat:@"%d" , self.presentCharDamage];
    self.TileWeaponName.text = @"";
    self.weaponLabel.text = self.tilWeaponName;
    
    self.pickUpWeapon.hidden = YES;
    
    
}

- (IBAction)actionButton:(id)sender {
    // need to add code to update boss battles
    
    if(self.bossHealth.hidden == NO){
        
        // Hiding the direction button , write logic to only hide appropriate buttons if boss is on the periphery tiles
        self.southButton.hidden =YES;
        self.northButton.hidden = YES;
        self.eastButton.hidden = YES;
        self.westButton.hidden =YES;
        
        int bossHealth = self.presentBossHealth; // getting the integer value
        int bossDamage = self.presentBossDamage;
        
        NSLog(@"init boss health  %d",bossHealth);
        
        // for simplicity I have the boss health + armor as a 1:1 ratio this could be modified to suit other ratios
        int characterHealth = self.presentCharHealth;
        int characterDamage = self.presentCharDamage;
        
        NSLog(@"init char health %d",characterHealth);
        
        self.bossHealth.text = [NSString stringWithFormat:@"%d" ,(bossHealth - characterDamage)];
        self.healthLabel.text = [NSString stringWithFormat:@"%d" , (characterHealth - bossDamage)];
        
        self.presentBossHealth = bossHealth - characterDamage;
        self.presentCharHealth = characterHealth - bossDamage;
        
        NSLog(@"final boss health  %d",self.presentBossHealth);
        NSLog(@"final char health %d",self.presentCharHealth);

    
        
        if(self.presentBossHealth<=0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BOSS DEFEATED" message:@"congrats" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"ContinueQuest",nil];
            [alert show];
            
            self.northButton.hidden=NO;
            self.southButton.hidden = NO;
            self.eastButton.hidden=NO;
            self.westButton.hidden=NO;
            
        }
        
        if(self.presentCharHealth <=0){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"YOU LOSE" message:@"Doom" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"ContinueQuest",nil];
            [alert show];
            
            self.northButton.hidden=NO;
            self.southButton.hidden = NO;
            self.eastButton.hidden=NO;
            self.westButton.hidden=NO;
            
        }
        
        
    }
    
    
    // need to add code to get the weapon and armor
    
    
    
    }
    @end
