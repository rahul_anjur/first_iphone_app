//
//  Character.h
//  myPirate
//
//  Created by Rahul Anjur on 5/4/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weapon.h"
#import "Armor.h"

@interface Character : NSObject

@property NSString *charName;
@property int charLife;
@property Weapon *presentWeapon;
@property Armor *presentArmor;

-(Character *) getInitChar;
@end
