//
//  XYZViewController.m
//  AgeOfLaika
//
//  Created by Rahul Anjur on 4/15/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import "XYZViewController.h"

@interface XYZViewController ()

@end

@implementation XYZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)convertButton:(id)sender {
    
    NSInteger age =[self.ageField.text  integerValue];
    NSInteger dogAge = age * 7 ;
    self.ageField.text = [NSString stringWithFormat:@"%d" , (int)dogAge];
    self.yearsLabel.text = @"Dog years age";
    
    
    
}
@end
