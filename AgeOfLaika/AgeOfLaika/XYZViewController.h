//
//  XYZViewController.h
//  AgeOfLaika
//
//  Created by Rahul Anjur on 4/15/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *ageField;
- (IBAction)convertButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *yearsLabel;

@end
