//
//  XYZAppDelegate.h
//  AgeOfLaika
//
//  Created by Rahul Anjur on 4/15/14.
//  Copyright (c) 2014 Rahul Anjur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
